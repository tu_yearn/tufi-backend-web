import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { UsersModule } from './modules/users/users.module';
import { TYPEORM_CONFIG } from './config/constants';
import databaseConfig from './config/database.config';
import { AppGateway } from './app.gateway';
import { CoinsModule } from './modules/coins/coins.module';
import { AdsModule } from './modules/ads/ads.module';
import { PaymentMethodsModule } from './modules/payment-methods/payment-methods.module';
import { BridgeCexsModule } from './modules/bridge-cexs/bridge-cexs.module';
import { ExchangesModule } from './modules/exchanges/exchanges.module';
import { NtfMarketplaceModule } from './modules/ntf-marketplace/ntf-marketplace.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../', 'tufi'),
      exclude: ['/api*'],
    }),
    TypeOrmModule.forRootAsync({
        inject: [ConfigService],
        useFactory: (config: ConfigService) =>
          config.get<TypeOrmModuleOptions>(TYPEORM_CONFIG),
      }),
    UsersModule,
    ConfigModule.forRoot({
        isGlobal: true,
        load: [databaseConfig],
        envFilePath: '.env',
      }),
      CoinsModule,
      AdsModule,
      PaymentMethodsModule,
      BridgeCexsModule,
      ExchangesModule,
      NtfMarketplaceModule
  ],
  controllers: [AppController],
  providers: [AppService, AppGateway],
})
export class AppModule {}
