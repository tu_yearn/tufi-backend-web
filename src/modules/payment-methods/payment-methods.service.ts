import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentMethod } from './entities/payment-method.entity';
import { Injectable } from '@nestjs/common';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { UpdatePaymentMethodDto } from './dto/update-payment-method.dto';

@Injectable()
export class PaymentMethodsService {
    constructor(
        @InjectRepository(PaymentMethod)
        private readonly pmRepository: Repository<PaymentMethod>
      ) {}

  async create(createPaymentMethodDto: CreatePaymentMethodDto[]): Promise<PaymentMethod[]> {
    const newPM = this.pmRepository.create(createPaymentMethodDto);
      await this.pmRepository.clear();
      const pm = await this.pmRepository.save(newPM);
      return pm;
  }

  async findAll(): Promise<PaymentMethod[]> {
    return await this.pmRepository.find({order: { description: 'ASC' },});
  }

  findOne(id: number) {
    return `This action returns a #${id} paymentMethod`;
  }

  update(id: number, updatePaymentMethodDto: UpdatePaymentMethodDto) {
    return `This action updates a #${id} paymentMethod`;
  }

  remove(id: number) {
    return `This action removes a #${id} paymentMethod`;
  }
}
