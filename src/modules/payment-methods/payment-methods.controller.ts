import { paymentMethodsData } from './../../config/data';
import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { PaymentMethodsService } from './payment-methods.service';
import { CreatePaymentMethodDto } from './dto/create-payment-method.dto';
import { UpdatePaymentMethodDto } from './dto/update-payment-method.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('PaymentMethods')
@Controller('payment-methods')
export class PaymentMethodsController {
  constructor(private readonly paymentMethodsService: PaymentMethodsService) {}

  @Post()
  async create(@Body() createPaymentMethodDto: CreatePaymentMethodDto[]) {
    createPaymentMethodDto = paymentMethodsData;
    const data =  await this.paymentMethodsService.create(createPaymentMethodDto);
    return {
        message: 'PaymentMethod created',
        data
    }
  }

  @Get()
  async findAll() {
    const data = await this.paymentMethodsService.findAll();
    return {
        message: 'Consulta exitosa',
        data
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentMethodsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updatePaymentMethodDto: UpdatePaymentMethodDto) {
    return this.paymentMethodsService.update(+id, updatePaymentMethodDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentMethodsService.remove(+id);
  }
}
