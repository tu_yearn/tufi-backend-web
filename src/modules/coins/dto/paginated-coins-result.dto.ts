import { Coin, DistintCoin } from '../entities/coin.entity';

export class PaginatedCoinsResultDto {
  data: Coin[] | DistintCoin[];
  page: number;
  limit: number;
  totalCount: number;
}
