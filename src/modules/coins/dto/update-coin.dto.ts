import { PartialType } from '@nestjs/mapped-types';
import { ReqCoinDto } from './req-coin.dto';

export class UpdateCoinDto extends PartialType(ReqCoinDto) {}
