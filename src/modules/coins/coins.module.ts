import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoinsService } from './coins.service';
import { CoinsController } from './coins.controller';
import { Coin } from './entities/coin.entity';

@Module({
  controllers: [CoinsController],
  providers: [CoinsService],
  imports: [TypeOrmModule.forFeature([Coin]), HttpModule],
  exports: [CoinsService]
})
export class CoinsModule {}
