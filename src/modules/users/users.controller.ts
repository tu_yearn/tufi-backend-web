import { SubscriptionUserDto } from './dto/subscription-user.dto';
import { Controller, Get, Post, Body, Put, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('USERS')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async findAll() {
    const data = await this.usersService.findAll();
    return {
        message: 'Consulta exitosa',
        data
    }
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const data = await this.usersService.findOne(id);
    return {
        message: 'Consulta existosa',
        data
    }
  }

  @Get('wallet/:address')
  async findOneWallet(@Param('address') address: string) {
    const data = await this.usersService.findOneWallet(address);
    return {
        message: 'Consulta existosa',
        data
    }
  }
  
  @Get('mail/:email')
  async findOneEmail(@Param('email') email: string) {
    const data = await this.usersService.findOneEmail(email);
    return {
        message: 'successful query',
        data
    }
  }
  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    const data =  await this.usersService.create(createUserDto);
    return {
        message: 'User created',
        data
    }
  }

  @Put(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() updateUserDto: UpdateUserDto) {
    const data = await this.usersService.update(+id, updateUserDto);
    return {
        message: 'User edited',
        data
    }
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    const data = await this.usersService.remove(id);
    return {
        message: 'User deleted',
        data
    }
  }

  @Post('/subscribe')
  async subscribe(@Body() subscriptionUserDto: SubscriptionUserDto) {
      console.log(subscriptionUserDto)
    const data =  await this.usersService.subcription(subscriptionUserDto);
    return {
        message: 'Subscribed User',
        data
    }
  }
}
