import { Wallet } from './wallet.entity';
import { ApiProperty } from '@nestjs/swagger';
import {
  //BeforeInsert,
  //BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
//import { hash } from 'bcryptjs';
import { Comment } from './comment.entity';

@Entity('users')
export class User {
  @ApiProperty()

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  firstname: string;

  @Column({ type: 'varchar', length: 255 })
  lastname: string;

  @Column({ type: 'varchar', length: 255, unique:true })
  email: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  referred: string;

  @Column({ type: 'boolean', default: false })
  status: boolean;

  @OneToMany(type => Comment, comment => comment.user )
  comment: Comment[];

  @OneToMany(type => Wallet, wallet => wallet.user )
  wallets: Wallet[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updateAt: Date;

  /* @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hash(this.password, 10);
  } */
}
