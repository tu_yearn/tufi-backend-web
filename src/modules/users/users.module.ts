import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User, Comment, Wallet } from './entities';

@Module({
  imports: [TypeOrmModule.forFeature([User, Comment, Wallet])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
