import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UpdateUserDto, CreateUserDto, SubscriptionUserDto } from './dto';
import { User, Comment } from './entities';
import { Wallet } from './entities/wallet.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    @InjectRepository(Wallet)
    private readonly walletRepository: Repository<Wallet>
  ) {}

  async findAll(): Promise<User[]> {
    return await this.userRepository.find({ relations: ["wallets"] });
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('Usuario no existe');
    return user;
  }

  async findOneWallet(address: string): Promise<User> {
    const user = await this.userRepository.createQueryBuilder("users")
    .leftJoinAndSelect("users.wallets", "wallet")
    .where("address = :a",{a: address})
    .getOne();
    if (!user) throw new NotFoundException('Usuariooooo no existe');
    return user;
  }

  async findOneEmail(email: string): Promise<User> {
    const user = await this.userRepository.createQueryBuilder("users")
    .leftJoinAndSelect("users.wallets", "wallet")
    .where("email = :a",{a: email})
    .getOne();
    if (!user) throw new NotFoundException('user does not exist');
    return user;
  }

  async create(dto: CreateUserDto): Promise<User> {
    const userExist = await this.userRepository.findOne({ email : dto.email });

    if (userExist) {
        const walletExist = await this.walletRepository.findOne({ address:  dto.address });
        const userWallets = await this.walletRepository.find({ user:  userExist });
        const editedUser = Object.assign(userExist, dto);
        if (!walletExist) {
          const newWallet = this.walletRepository.create({ address:  dto.address });
          const wallet = await this.walletRepository.save(newWallet);
          userWallets.push(wallet);
          editedUser.wallets = userWallets;
          const user = await this.userRepository.save(editedUser);
          return user;
        } else {
          editedUser.wallets = userWallets;
          const user = await this.userRepository.save(editedUser);
          return user;
        }
    } else {
        const newWallet = this.walletRepository.create({ address:  dto.address });
        await this.walletRepository.save(newWallet);

        const newUser = this.userRepository.create(dto);
        newUser.wallets = [newWallet];
        const user = await this.userRepository.save(newUser);
        return user;
    }
  }

  async update(id: number, dto: UpdateUserDto): Promise<User> {
    const userEdit = await this.findOne(id);

    const editedUser = Object.assign(userEdit, dto);
    const user = await this.userRepository.save(editedUser);
    return user;
  }

  async remove(id: number) {
    const user = await this.findOne(id);
    return await this.userRepository.remove(user);
  }

  async emailExist(user: CreateUserDto) {
    return await this.userRepository.findOne({ email : user.email });
  }

  async subcription(dto: SubscriptionUserDto): Promise<User> {
    const userExist = await this.userRepository.findOne({ email : dto.email });
    let user: User;

    if (!userExist) {
        const newUser = this.userRepository.create({
            firstname: dto.firstname,
            lastname: dto.lastname,
            email: dto.email,
            status: false
        });
        user = await this.userRepository.save(newUser);
    }

    /*const newComment = this.commentRepository.create({
        title: dto.title,
        comment: dto.comment,
        user: user
    });
    await this.commentRepository.save(newComment);*/
    return user;
  }
}
