import { IsEmail, IsOptional, IsString, MaxLength } from "class-validator";

export class SubscriptionUserDto {

  @IsString()
  @MaxLength(255)
  firstname: string;

  @IsString()
  @MaxLength(255)
  lastname: string;

  @IsEmail()
  @MaxLength(255)
  email: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  title: string;

  @IsOptional()
  @IsString()
  comment: string;

  @IsString()
  @MaxLength(255)
  address: string;

}
