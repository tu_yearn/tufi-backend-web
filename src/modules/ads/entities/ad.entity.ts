import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('ads')
export class Ad {
  @ApiProperty()
  @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  name: string;

  @Column({ type: 'integer', nullable: true })
  feedback_score: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  trade_count: string;

  @Column({
    type: 'timestamp',
    nullable: true,
    transformer: {
      from: (value?: Date | null) =>
        value === undefined || value === null ? value : value.toISOString(),
      to: (value?: string | null) =>
        value === undefined || value === null ? value : new Date(value),
    },
  })
  last_online: Date;

  @Column({ type: 'boolean', nullable: true })
  visible: boolean;

  @Column({ type: 'float', nullable: true })
  temp_price: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  online_provider: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  bank_name: string;

  @Column({ type: 'float', default: 0.0 })
  min_amount: number;

  @Column({ type: 'float', default: 0.0 })
  max_amount: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  location_string: string;

  @Column({ type: 'smallint', nullable: true })
  payment_window_minutes: number;

  @Column({ type: 'text', nullable: true })
  msg: string;

  @Column({
    type: 'timestamp',
    nullable: true,
    transformer: {
      from: (value?: Date | null) =>
        value === undefined || value === null ? value : value.toISOString(),
      to: (value?: string | null) =>
        value === undefined || value === null ? value : new Date(value),
    },
  })
  created_at: Date;

  @Column({ type: 'varchar', length: 255, nullable: true })
  public_view: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  countrycode: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  trade_type: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  fiat_currency: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  coin_currency: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  provider: string;
}
