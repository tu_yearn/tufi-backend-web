import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdsService } from './ads.service';
import { AdsController } from './ads.controller';
import { Ad } from './entities/ad.entity';

@Module({
  controllers: [AdsController],
  providers: [AdsService],
  imports: [TypeOrmModule.forFeature([Ad]), HttpModule],
  exports: [AdsService]
})
export class AdsModule {}
