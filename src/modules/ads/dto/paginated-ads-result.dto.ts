import { Ad } from '../entities/ad.entity';

export class PaginatedAdsResultDto {
  data: Ad[];
  page: number;
  limit: number;
  totalCount: number;
}
