import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  Injectable,
  HttpService,
  Logger,
  NotFoundException,
} from '@nestjs/common';

import { tokenLocalCoinSwap, urlPriceLocalCloinSwap } from 'src/config/constants';
import { UpdateAdDto } from './dto/update-ad.dto';
import { Ad } from './entities/ad.entity';
import { PaginatedAdsResultDto, PaginationDto } from './dto';

const logger = new Logger('AdServices');

@Injectable()
export class AdsService {
  constructor(
    @InjectRepository(Ad)
    private readonly adRepository: Repository<Ad>,
    private httpService: HttpService,
  ) {}

  async findAll(paginationDto: PaginationDto): Promise<PaginatedAdsResultDto> {
    try {
        let priceMarket = 0;
        if (paginationDto.fiat_currency !== "ETH") {
            priceMarket = await this.getPriceMarket(paginationDto.fiat_currency, paginationDto.coin_currency);
        }
      logger.log('Precio market:' + priceMarket);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const direction =
        paginationDto.sortOrderDirection === 'DESC' ? 'DESC' : 'ASC';
      const name = paginationDto.sortOrderName;
      const parameters = paginationDto.filter;
      const fiat_currency = paginationDto.fiat_currency;
      const coin_currency = paginationDto.coin_currency;
      const countrycode = paginationDto.countrycode;
      const textSearch = paginationDto.textSearch;
      const online_provider = paginationDto.online_provider;

      const result = this.adRepository.createQueryBuilder();
      result.where(parameters);
      if (textSearch != '') {
        result.andWhere('bank_name LIKE :m', {
          m: `%${textSearch}%`,
        });
      }
      if (fiat_currency != '') {
        result.andWhere('fiat_currency = :x', { x: fiat_currency });
      }
      if (coin_currency != '') {
        result.andWhere('coin_currency = :xx', { xx: coin_currency });
      }
      if (countrycode != '') {
        result.andWhere('countrycode = :y', { y: countrycode });
      }
      if (online_provider != '') {
        result.andWhere('online_provider LIKE :z', { z: `%${online_provider}%` });
      }
      if (priceMarket != 0) {
        result.andWhere('temp_price BETWEEN :a AND :b', {
            a: priceMarket * 0.8,
            b: priceMarket * 1.2,
        });
      }
      result.orderBy(name, direction);
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const ads = await result.getMany();
      const count = await result.getCount();
      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: ads,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  async findOne(id: number): Promise<Ad> {
    const ad = await this.adRepository.findOne(id);
    if (!ad) throw new NotFoundException('Ads no existe');
    return ad;
  }

  update(id: number, updateAdDto: UpdateAdDto) {
    return `This action updates a #${id} ad`;
  }

  remove(id: number) {
    return `This action removes a #${id} ad`;
  }

  async getPriceMarket(fiat_currency: string, coin_currency: string): Promise<number> {
    const headersRequest = {
        'Content-Type': 'application/json', // afaik this one is not needed
        'Authorization': `Token ${tokenLocalCoinSwap}`,
    };
    const raw = {
        "pricing_market": "binance",
        "coin_currency": coin_currency.toUpperCase(),
        "fiat_currency": fiat_currency.toUpperCase(),
        "margin_percent": "0"
    };

    try {
        const res = await this.httpService
        .post(urlPriceLocalCloinSwap, raw, { headers: headersRequest }).toPromise();
        if (res.data === '') {
            return 0;
        }
        const price: number = res.data.price;
        return price;
    } catch (error) {
        logger.error(error);
        return 0;
    }

  }
}
