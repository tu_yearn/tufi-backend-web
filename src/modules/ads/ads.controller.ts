import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Logger,
  Query,
} from '@nestjs/common';
import { AdsService } from './ads.service';
import { UpdateAdDto } from './dto/update-ad.dto';
import { PaginatedAdsResultDto, PaginationDto } from './dto';

const logger = new Logger('AdController');

@Controller('ads')
export class AdsController {
  constructor(private readonly adsService: AdsService) {}

  @Post()
  async create() {
    logger.log('Actualizando tabla Ads');
    return {
      message: 'Table Ads created',
      data: [],
    };
  }

  @Get()
  async findAll(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('sortOrderName') sortOrderName: string,
    @Query('sortOrderDirection') sortOrderDirection: string,
    @Query('filter') filter: string,
    @Query('fiat_currency') fiat_currency: string,
    @Query('coin_currency') coin_currency: string,
    @Query('countrycode') countrycode: string,
    @Query('online_provider') online_provider: string,
    @Query('textSearch') textSearch: string,

  ): Promise<PaginatedAdsResultDto> {
    const paginationDto: PaginationDto = {
      page,
      limit,
      sortOrderName,
      sortOrderDirection,
      filter,
      fiat_currency,
      coin_currency,
      countrycode,
      online_provider,
      textSearch
    };
    console.log(paginationDto);
    return this.adsService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.adsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateAdDto: UpdateAdDto) {
    return this.adsService.update(+id, updateAdDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adsService.remove(+id);
  }
}
