import { Controller, Get, Post, Body, Put, Param, Delete, Query } from '@nestjs/common';
import { BridgeCexsService } from './bridge-cexs.service';
import { CreateCurrencyDto, CreateProviderDto, PaginatedCoinsResultDto, PaginationDto, UpdateCurrencyDto, UpdateProviderDto } from './dto';
import { currencyData } from 'src/config/data'
import { BridgeCex } from './entities';

@Controller('bridge-cexs')
export class BridgeCexsController {
  constructor(private readonly bridgeCexsService: BridgeCexsService) {}

  @Post()
  async create(): Promise<BridgeCex[]> {
    /* const coin = ['ZRX','1INCH','AAVE','ALGO','FORTH','ANKR','REP','BAL','BNT','BAND','BAT','BTC','BCH','ADA','CTSI','CGLD','LINK','CVC','COMP','ATOM','CRV','DAI','DASH','MANA','EOS','ENJ','ETH','ETC','FIL','ICP','KNC','LTC','LRC','MKR','MIR','NKN','NU','NMR','OMG','OXT','OGN','MATIC','REN','SKL','XLM','STORJ','SUSHI','SNX','TRB','USDT','XTZ','GRT','UMA','USDC','UNI','WBTC','XRP','ZEC','DNT','RLC','YFI'];
    const dto1 = await this.bridgeCexsService.getGlufcoRates();
    await this.bridgeCexsService.create(dto1);
    const dto2 = await this.bridgeCexsService.getBinanceRates();
    const rest = await this.bridgeCexsService.create(dto2);
    coin.forEach(async (element) => {
        const dto3 = await this.bridgeCexsService.getCoinbaseRates(element);
        await this.bridgeCexsService.create(dto3);
    });
    const rest = await this.CreateBridgeCexsBitfinex();
    const dto = await this.bridgeCexsService.getCriptolagoRates();
    const rest = await this.bridgeCexsService.create(dto);
    const dto = await this.bridgeCexsService.getGeminiRates();
    const rest = await this.bridgeCexsService.create(dto);*/
    const dto = await this.bridgeCexsService.getUpholdRates();
    const rest = await this.bridgeCexsService.create(dto);
    return rest;
  }

  @Get()
  findAll(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('filter') filter: string,
  ): Promise<PaginatedCoinsResultDto> {
    const paginationDto: PaginationDto = {
        page,
        limit,
        filter
      };
    console.log(paginationDto);
    return this.bridgeCexsService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return id
  }

  @Put()
  async update() {
    const dto = await this.bridgeCexsService.getGlufcoRates();
    return await this.bridgeCexsService.update(dto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bridgeCexsService.remove(+id);
  }

  // CURRENCIES

  @Get('currency/all')
  async findAllCurrency() {
    return await this.bridgeCexsService.findAllCurrencies();
  }

  @Post('currency')
  async createCurrency() {
    const createCurrencyDto: CreateCurrencyDto[] = currencyData;
    const data =  await this.bridgeCexsService.createCurrency(createCurrencyDto);
    return {
        message: 'Currency created',
        data
    }
  }

  @Put('currency/:country_code')
  async updateCurrency(@Param('country_code') country_code: string, @Body() dto: UpdateCurrencyDto) {
    return await this.bridgeCexsService.updateCurrency(country_code, dto);
  }

  // PROVIDERS

  @Get('provider/all')
  async findAllProvider() {
    return await this.bridgeCexsService.findAllProviders();
  }

  @Post('provider')
  async createProvider(@Body() dto: CreateProviderDto) {
    const data =  await this.bridgeCexsService.createProvider(dto);
    return {
        message: 'Provider created',
        data
    }
  }

  @Put('provider/:id')
  async updateProvider(@Param('id') id: string, @Body() dto: UpdateProviderDto) {
    return await this.bridgeCexsService.updateProvider(id, dto);
  }

  async CreateBridgeCexsBitfinex() {
    let results: any[];
    const coin = ['BTC','ETH','TRX','BAT','ALG'];
      coin.forEach(async (element) => {
        const currency = ['USD','EUR','CNH','JPY','GBP'];
        let res = [];
        for (let index = 0; index < currency.length; index++) {
            const cur = currency[index];
            const dto = await this.bridgeCexsService.getBitfinexRates(element, cur);
            res = res.concat(await dto);
        }
        results = await Promise.all(res);
        await this.bridgeCexsService.create(res);
      });
    return results;
  }

}
