import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  urlGetGlufcoRates,
  urlGetCurrenciesRates,
  urlGetBinanceRates,
  urlGetGeminiRates,
  urlCoinbaseRates,
  urlBitfinexRates,
  urlCriptolagoRates,
  urlUpholdRates
} from 'src/config/constants';
import { getRepository, Repository } from 'typeorm';

import {
  ListCoinsGlufco,
  ListCurrenciesRates,
  ListCoinsBinance,
  ListCoinsGemini,
  ListCoinsCoinbase,
  ListCoinsBitfinex,
  ListCoinsCriptolago,
  ListCoinsUphold
} from 'src/models';
import {
  CreateCurrencyDto,
  CreateBridgeCexDto,
  UpdateBridgeCexDto,
  UpdateCurrencyDto,
  CreateProviderDto,
  UpdateProviderDto,
  PaginationDto,
  PaginatedCoinsResultDto,
} from './dto';
import { Currency, Provider, BridgeCex, viewBridgeCexs } from './entities';
import { Coin } from './../coins/entities/coin.entity';
const logger = new Logger('BridgeCexsServices');

@Injectable()
export class BridgeCexsService {
  constructor(
    private httpService: HttpService,
    @InjectRepository(Currency)
    private readonly currencyRepository: Repository<Currency>,
    @InjectRepository(Provider)
    private readonly providerRepository: Repository<Provider>,
    @InjectRepository(BridgeCex)
    private readonly bridgeCexRepository: Repository<BridgeCex>,
    @InjectRepository(Coin)
    private readonly coinRepository: Repository<Coin>,
  ) {}

  async create(dto: CreateBridgeCexDto[]): Promise<BridgeCex[]> {
    try {
      const newBridge = dto.map(glf => this.bridgeCexRepository.create(glf));
      const bridge = await this.bridgeCexRepository.save(newBridge);
      logger.log('Created BridgeCex');
      return bridge;
    } catch (error) {
      console.log(error.message);
      return [];
    }
  }

  async findAll(
    paginationDto: PaginationDto,
  ): Promise<PaginatedCoinsResultDto> {
    try {
      const viewBridgeCexsRepository = getRepository(viewBridgeCexs);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const parameters =
        paginationDto.filter === '' ? null : paginationDto.filter;

      const result = viewBridgeCexsRepository.createQueryBuilder();
      result.where(parameters);
      result.orderBy('market_cap_rank', 'ASC');
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const coins = await result.getMany();
      const count = await result.getCount();

      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: coins,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  async findOne(dto: UpdateBridgeCexDto): Promise<BridgeCex> {
    return await this.bridgeCexRepository.findOne(dto);
  }

  async update(dto: UpdateBridgeCexDto[]): Promise<any> {
    try {
      dto.forEach(glf => {
        this.bridgeCexRepository
          .findOne({
            coin: glf.coin,
            country: glf.country,
            provider: glf.provider,
          })
          .then(b => {
            if (b) {
              const editedBridgeCex = Object.assign(b, glf);
              this.bridgeCexRepository.save(editedBridgeCex);
            }
          });
      });
      logger.log('UpdateBridgeCexDto ');
      return { message: 'Actualizado' };
    } catch (error) {
      console.log(error.message);
      return 'Error';
    }
  }

  remove(id: number) {
    return `This action removes a #${id} bridgeCex`;
  }

  //COINS
  async findAllCoins(): Promise<Coin[]> {
    const coin = await this.coinRepository.find();
    return coin;
  }

  //CURRENCIES

  async findAllCurrencies(): Promise<Currency[]> {
    const cur = await this.currencyRepository.find();
    return cur;
  }

  async createCurrency(
    createCurrencyDto: CreateCurrencyDto[],
  ): Promise<Currency[] | any> {
    try {
      const newCur = this.currencyRepository.create(createCurrencyDto);
      const cur = await this.currencyRepository.save(newCur);
      return cur;
    } catch (error) {
      console.log(error.message);
      return { error: 'Error interno del sistema' };
    }
  }

  async updateCurrency(
    country_code: string,
    dto: UpdateCurrencyDto,
  ): Promise<Currency | any> {
    const currencyEdit = await this.currencyRepository.findOne({
      country_code,
    });
    try {
      if (currencyEdit) {
        const editedCurrency = Object.assign(currencyEdit, dto);
        const currency = await this.currencyRepository.save(editedCurrency);
        return currency;
      } else {
        return { message: 'No encontrado' };
      }
    } catch (error) {
      console.log(error.message);
      return { error: 'Error interno del sistema' };
    }
  }

  async updateCurrencyRate(dto: UpdateCurrencyDto[]): Promise<any> {
    try {
      dto.forEach(cur => {
        this.currencyRepository
          .find({
            currency_symbol: cur.currency_symbol,
          })
          .then(b => {
            if (b) {
              b.map(c => {
                const editedCurrency = Object.assign(c, cur);
                this.currencyRepository.save(editedCurrency);
              });
            }
          });
      });
      console.log('UpdateCurrencyRate ');
      return { message: 'Actualizado' };
    } catch (error) {
      console.log(error.message);
      return 'Error';
    }
  }

  //PROVIDERS

  async findAllProviders(): Promise<Provider[]> {
    const cur = await this.providerRepository.find();
    return cur;
  }

  async createProvider(dto: CreateProviderDto): Promise<Provider | any> {
    try {
      const newPro = this.providerRepository.create(dto);
      const cur = await this.providerRepository.save(newPro);
      return cur;
    } catch (error) {
      console.log(error.message);
      return { error: 'Error interno del sistema' };
    }
  }

  async updateProvider(
    id: string,
    dto: UpdateProviderDto,
  ): Promise<Provider | any> {
    const providerEdit = await this.providerRepository.findOne(id);
    try {
      if (providerEdit) {
        const editedProvider = Object.assign(providerEdit, dto);
        const provider = await this.providerRepository.save(editedProvider);
        return provider;
      } else {
        return { message: 'No encontrado' };
      }
    } catch (error) {
      console.log(error.message);
      return { error: 'Error interno del sistema' };
    }
  }

  async getGlufcoRates(): Promise<CreateBridgeCexDto[]> {
    try {
      const res = await this.httpService.get(urlGetGlufcoRates).toPromise();
      const dto = ListCoinsGlufco.adListJson(res.data);

      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getCurrencyRates(): Promise<ListCurrenciesRates[]> {
    try {
      const res = await this.httpService.get(urlGetCurrenciesRates).toPromise();
      const dto = ListCurrenciesRates.adListJson(res.data);
      return dto;
    } catch (error) {
      logger.error(error.message);
      return null;
    }
  }

  async getBinanceRates(): Promise<any> {
    try {
      const cur: CreateCurrencyDto[] = await this.findAllCurrencies();
      const res = await this.httpService.get(urlGetBinanceRates).toPromise();
      const dto = ListCoinsBinance.adListJson(res.data, cur);
      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getCoinbaseRates(coin: string): Promise<any> {
    try {
      const res = await this.httpService.get(urlCoinbaseRates+coin).toPromise();
      const dto = ListCoinsCoinbase.adListJson(res.data, coin);
      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getBitfinexRates(coin: string, cur: string): Promise<any> {
    try {
            const data = {
                ccy1: coin,
                ccy2: cur
            }
            const res = await this.httpService.post(urlBitfinexRates, data).toPromise();
            const dto = ListCoinsBitfinex.adListJson(res.data[0], coin, cur);
      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getCriptolagoRates(): Promise<CreateBridgeCexDto[]> {
    try {
      const res = await this.httpService.get(urlCriptolagoRates).toPromise();
      const dto = ListCoinsCriptolago.adListJson(res.data);

      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getGeminiRates(): Promise<any> {
    try {
      const cur: CreateCurrencyDto[] = await this.findAllCurrencies();
      const res = await this.httpService.get(urlGetGeminiRates).toPromise();
      const dto = ListCoinsGemini.adListJson(res.data, cur);
      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

  async getUpholdRates(): Promise<CreateBridgeCexDto[]> {
    try {
      const cur: CreateCurrencyDto[] = await this.findAllCurrencies();
      const coin = await this.findAllCoins();
      const res = await this.httpService.get(urlUpholdRates).toPromise();
      const dto = ListCoinsUphold.adListJson(res.data,cur,coin);

      return dto;
    } catch (error) {
      logger.error(error.message);
      return [];
    }
  }

}
