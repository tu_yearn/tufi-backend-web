import { IsString, MaxLength } from "class-validator";

export class CreateProviderDto {

  @IsString()
  @MaxLength(255)
  description: string;

  @IsString()
  @MaxLength(255)
  url_logo: string;

}
