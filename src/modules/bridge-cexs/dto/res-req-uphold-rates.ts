export interface ResReqUpholdRates {
    ask:      string;
    bid:      string;
    currency: string;
    pair:     string;
}
