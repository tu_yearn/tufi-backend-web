import { PartialType } from '@nestjs/mapped-types';
import { CreateBridgeCexDto } from './create-bridge-cex.dto';

export class UpdateBridgeCexDto extends PartialType(CreateBridgeCexDto) {}
