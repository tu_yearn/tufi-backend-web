import { IsNumber, IsOptional, IsString, MaxLength } from "class-validator";

export class CreateCurrencyDto {

  @IsString()
  @MaxLength(255)
  country_code: string;

  @IsString()
  @MaxLength(255)
  country_name: string;

  @IsString()
  @MaxLength(255)
  currency_symbol: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  currency_name?: string;

  @IsOptional()
  @IsNumber()
  rate_usd?: number;

  @IsOptional()
  @IsNumber()
  rate_eur?: number;

}
