export interface ResReqGeminiRates {
    pair:             string;
    price:            string;
    percentChange24h: string;
}
