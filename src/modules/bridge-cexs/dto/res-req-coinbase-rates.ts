export interface ResReqCoinbaseRates {
    data: Data;
}

export interface Data {
    currency: string;
    rates:    { [key: string]: string };
}
