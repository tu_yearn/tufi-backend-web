export interface ResGetGlufcoRatesInterface {
    glf:               string;
    ves_glf_buy_2pct:  string;
    ves_glf_sell_1pct: string;
    zelle_glf:         string;
    pen_glf:           string;
    clp_glf:           string;
    ecu_glf:           string;
    brl_glf:           string;
}
