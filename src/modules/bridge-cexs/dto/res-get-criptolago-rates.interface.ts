export interface ResGetCriptolagoRatesInterface {
    ves_btc:  string;
    btc_ves:  string;
    btc_ptr:  string;
    ptr_btc:  string;
    ves_ptr:  string;
    ptr_ves:  string;
    btc_glf:  string;
    glf_btc:  string;
    ves_glf:  string;
    glf_ves:  string;
    btc_ltc:  string;
    ltc_btc:  string;
    ves_ltc:  string;
    ltc_ves:  string;
    btc_dash: string;
    dash_btc: string;
    ves_dash: string;
    dash_ves: string;
    usd_ptr:  string;
    usd_btc:  string;
    usd_glf:  string;
}
