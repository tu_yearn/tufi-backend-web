import { BridgeCex, viewBridgeCexs } from '../entities';

export class PaginatedCoinsResultDto {
  data: BridgeCex[] | viewBridgeCexs[];
  page: number;
  limit: number;
  totalCount: number;
}
