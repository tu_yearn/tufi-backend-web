import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateBridgeCexDto {
  @IsString()
  coin: string;

  @IsString()
  country: string;

  @IsNumber()
  tasa: number;

  @IsNumber()
  provider: number;

  @IsString()
  url_provider: string;

  @IsBoolean()
  stable_coin: boolean;
}
