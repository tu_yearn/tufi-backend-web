import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule, Module } from '@nestjs/common';

import { BridgeCexsService } from './bridge-cexs.service';
import { BridgeCexsController } from './bridge-cexs.controller';
import { BridgeCex, Provider, Currency } from './entities';
import { Coin } from '../coins/entities/coin.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([BridgeCex, Provider, Currency, Coin]),
        HttpModule
    ],
  controllers: [BridgeCexsController],
  providers: [BridgeCexsService],
  exports: [BridgeCexsService]
})
export class BridgeCexsModule {}
