import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('currencies')
export class Currency {
  @ApiProperty()

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  country_code: string;

  @Column({ type: 'varchar', length: 255 })
  country_name: string;

  @Column({ type: 'varchar', length: 255 })
  currency_symbol: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  currency_name: string;

  @Column({ type: 'double', nullable: true })
  rate_usd: number;

  @Column({ type: 'double', nullable: true })
  rate_eur: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updateAt: Date;

}
