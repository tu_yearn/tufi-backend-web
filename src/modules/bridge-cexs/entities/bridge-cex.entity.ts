import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ViewColumn,
  ViewEntity,
} from 'typeorm';

@Entity('bridge_cexs')
export class BridgeCex {
  @ApiProperty()

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  coin: string;

  @Column({ type: 'varchar', length: 255 })
  country: string;

  @Column({ type: 'double' })
  tasa: number;

  @Column({ type: 'varchar', length: 255 })
  provider: number;

  @Column({ type: 'varchar', length: 255 })
  url_provider: string;

  @Column({ type: 'boolean', default: true })
  stable_coin: boolean;

  @Column({ type: 'boolean', default: true })
  status: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updateAt: Date;

}

@ViewEntity({
    expression: `
    SELECT
    a.id,
    a.coin,
    a.tasa,
    b.description as provider,
    a.url_provider,
    b.url_logo as logo_provider,
    c.currency_symbol,
    c.currency_name,
    c.country_code,
    d.image,
    d.name as name_coin,
    d.pk as market_cap_rank,
    a.stable_coin

    FROM tufi.bridge_cexs a
    JOIN providers b ON a.provider = b.id
    JOIN currencies c ON a.country = c.country_code
    LEFT JOIN coins d ON a.coin = UPPER(d.symbol)
    where (d.name != 'Gallery Finance' OR d.name != 'Binance-Peg Litecoin') AND b.published_at IS NOT NULL
    `
})
// a.status = true AND
export class viewBridgeCexs {

    @ViewColumn()
    id: number;

    @ViewColumn()
    coin: string;

    @ViewColumn()
    name_coin: string;

    @ViewColumn()
    tasa: number;

    @ViewColumn()
    provider: string;

    @ViewColumn()
    url_provider: string;

    @ViewColumn()
    logo_provider: string;

    @ViewColumn()
    currency_symbol: string;

    @ViewColumn()
    currency_name: string;

    @ViewColumn()
    country_code: string;

    @ViewColumn()
    image: string;

    @ViewColumn()
    market_cap_rank: string;

    @ViewColumn()
    stable_coin: boolean;

}
