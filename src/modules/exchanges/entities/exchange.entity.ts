import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
  } from "typeorm";

  @Entity("exchanges")
  export class Exchange {
    @PrimaryGeneratedColumn({ type: "int", unsigned: true })
    id: number;

    @Column({ type: "varchar", nullable: true })
    uid: string;

    @Column({ type: "varchar", nullable: true })
    name: string;

    @Column({ type: "varchar", nullable: true })
    logo: string;

    @Column({ type: "varchar", nullable: true })
    url: string;

    @Column({ type: "double", nullable: true })
    volume_24h: number;

    @Column({ type: "int", nullable: true })
    num_coins: number;

    @Column({ type: "int", nullable: true })
    num_pairs: number;

    @Column({ type: "int", nullable: true })
    visits: number;

    @Column({ type: "varchar", nullable: true })
    most_traded_pair: string;

    @Column({ type: "double", nullable: true })
    vol_most_traded_pair: number;

    @Column({ type: "varchar", nullable: true })
    logo_most_traded: string;

    @Column({ type: "double", nullable: true })
    market_share_by_volume: number;

    @Column({ type: "datetime", nullable: true })
    published_at: Date;

    @Column({ type: "int", nullable: true })
    created_by: number;

    @Column({ type: "int", nullable: true })
    updated_by: number;

    @Column({ type: "timestamp", default: () => 'CURRENT_TIMESTAMP', nullable: true })
    created_at: Date;

    @Column({ type: "timestamp", default: () => 'CURRENT_TIMESTAMP', nullable: true })
    updated_at: Date;
  }
