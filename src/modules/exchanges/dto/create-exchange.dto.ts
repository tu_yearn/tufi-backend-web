import { IsNumber, IsOptional, IsString, MaxLength } from "class-validator";

export class CreateExchangeDto {

  @IsOptional()
  @IsString()
  @MaxLength(255)
  uid: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  name: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  logo: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  url: string;

  @IsOptional()
  @IsNumber()
  volume_24h: number;

  @IsOptional()
  @IsNumber()
  num_coins: number;

  @IsOptional()
  @IsNumber()
  num_pairs: number;

  @IsOptional()
  @IsNumber()
  visits: number;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  most_traded_pair: string;

  @IsOptional()
  @IsNumber()
  vol_most_traded_pair: number;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  logo_most_traded: string;

  @IsOptional()
  @IsNumber()
  market_share_by_volume: number;
}
