import { Injectable, Logger } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { PaginatedNtfMarketplaceResultDto, PaginationDto } from './dto';
import { CreateNtfMarketplaceDto } from './dto/create-ntf-marketplace.dto';
import { UpdateNtfMarketplaceDto } from './dto/update-ntf-marketplace.dto';
import { NtfMarketplace } from './entities/ntf-marketplace.entity';

const logger = new Logger("ExchangeServices");

@Injectable()
export class NtfMarketplaceService {
  create(createNtfMarketplaceDto: CreateNtfMarketplaceDto) {
    return 'This action adds a new ntfMarketplace';
  }

  async findAll(
    paginationDto: PaginationDto
  ): Promise<PaginatedNtfMarketplaceResultDto> {
    try {
      const distintCoinRepository = getRepository(NtfMarketplace);
      const skippedItems = (paginationDto.page - 1) * paginationDto.limit;
      const direction =
        paginationDto.sortOrderDirection === "DESC" ? "DESC" : "ASC";
      const name = paginationDto.sortOrderName;
      const parameters = paginationDto.filter;
      const textSearch = paginationDto.textSearch ? paginationDto.textSearch : '';

      const result = distintCoinRepository.createQueryBuilder();
      result.where(parameters);
      result.andWhere('published_at IS NOT NULL');
      if (textSearch!=='') {result.andWhere('name LIKE :x',{ x: `%${textSearch}%` });}
      if (textSearch!=='') {result.orWhere('categories LIKE :y',{ y: `%${textSearch}%` });}
      if (textSearch!=='') {result.orWhere('blockchain LIKE :z',{ z: `%${textSearch}%` });}
      result.orderBy(name, direction);
      result.offset(skippedItems);
      result.limit(paginationDto.limit);
      const coins = await result.getMany();
      const count = await result.getCount();

      return {
        totalCount: count,
        page: paginationDto.page,
        limit: paginationDto.limit,
        data: coins,
      };
    } catch (error) {
      logger.error(error);
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} ntfMarketplace`;
  }

  update(id: number, updateNtfMarketplaceDto: UpdateNtfMarketplaceDto) {
    return `This action updates a #${id} ntfMarketplace`;
  }

  remove(id: number) {
    return `This action removes a #${id} ntfMarketplace`;
  }
}
