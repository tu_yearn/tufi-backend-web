import { Controller, Get, Post, Body, Put, Param, Delete, Query } from '@nestjs/common';
import { NtfMarketplaceService } from './ntf-marketplace.service';
import { CreateNtfMarketplaceDto } from './dto/create-ntf-marketplace.dto';
import { UpdateNtfMarketplaceDto } from './dto/update-ntf-marketplace.dto';
import { PaginatedNtfMarketplaceResultDto, PaginationDto } from './dto';

@Controller('ntf-marketplace')
export class NtfMarketplaceController {
  constructor(private readonly ntfMarketplaceService: NtfMarketplaceService) {}

  @Post()
  create(@Body() createNtfMarketplaceDto: CreateNtfMarketplaceDto) {
    return this.ntfMarketplaceService.create(createNtfMarketplaceDto);
  }

  @Get()
  async findAll(
    @Query("page") page = 1,
    @Query("limit") limit = 100,
    @Query("sortOrderName") sortOrderName = "market_share_by_volume",
    @Query("sortOrderDirection") sortOrderDirection = "DESC",
    @Query("filter") filter: string,
    @Query("textSearch") textSearch: string
  ): Promise<PaginatedNtfMarketplaceResultDto> {
    const paginationDto: PaginationDto = {
      page,
      limit,
      sortOrderName,
      sortOrderDirection,
      filter,
      textSearch
    };
    console.log(paginationDto);
    return await this.ntfMarketplaceService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 250 ? 250 : paginationDto.limit,
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ntfMarketplaceService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateNtfMarketplaceDto: UpdateNtfMarketplaceDto) {
    return this.ntfMarketplaceService.update(+id, updateNtfMarketplaceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ntfMarketplaceService.remove(+id);
  }
}
