import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NtfMarketplaceService } from './ntf-marketplace.service';
import { NtfMarketplaceController } from './ntf-marketplace.controller';
import { NtfMarketplace } from './entities/ntf-marketplace.entity';

@Module({
  imports: [TypeOrmModule.forFeature([NtfMarketplace])],
  controllers: [NtfMarketplaceController],
  providers: [NtfMarketplaceService]
})
export class NtfMarketplaceModule {}
