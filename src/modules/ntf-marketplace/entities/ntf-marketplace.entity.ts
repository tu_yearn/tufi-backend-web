import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
  } from "typeorm";

  @Entity("ntf_marketplaces")
  export class NtfMarketplace {
    @PrimaryGeneratedColumn({ type: "int", unsigned: true })
    id: number;

    @Column({ type: "varchar", nullable: true })
    name: string;

    @Column({ type: "int", nullable: true })
    num_ntfs: number;

    @Column({ type: "varchar", nullable: true })
    blockchain: string;

    @Column({ type: "varchar", nullable: true })
    categories: string;

    @Column({ type: "decimal", precision: 10, scale: 2 , nullable: true })
    fee: number;

    @Column({ type: "int", nullable: true })
    active_since: number;

    @Column({ type: "varchar", nullable: true })
    type: string;

    @Column({ type: "varchar", nullable: true })
    logo: string;

    @Column({ type: "varchar", nullable: true })
    url: string;

    @Column({ type: "datetime", nullable: true })
    published_at: Date;

    @Column({ type: "int", nullable: true })
    created_by: number;

    @Column({ type: "int", nullable: true })
    updated_by: number;

    @Column({ type: "timestamp", default: () => 'CURRENT_TIMESTAMP', nullable: true })
    created_at: Date;

    @Column({ type: "timestamp", default: () => 'CURRENT_TIMESTAMP', nullable: true })
    updated_at: Date;
  }
