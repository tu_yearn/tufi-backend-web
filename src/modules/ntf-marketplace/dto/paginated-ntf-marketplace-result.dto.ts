import { NtfMarketplace } from './../entities/ntf-marketplace.entity';

export class PaginatedNtfMarketplaceResultDto {
  data: NtfMarketplace[];
  page: number;
  limit: number;
  totalCount: number;
}
