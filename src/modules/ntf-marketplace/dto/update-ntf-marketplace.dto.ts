import { PartialType } from '@nestjs/mapped-types';
import { CreateNtfMarketplaceDto } from './create-ntf-marketplace.dto';

export class UpdateNtfMarketplaceDto extends PartialType(CreateNtfMarketplaceDto) {}
