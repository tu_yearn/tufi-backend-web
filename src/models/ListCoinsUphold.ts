import { Coin } from './../modules/coins/entities/coin.entity';
import { ResReqUpholdRates, CreateCurrencyDto } from 'src/modules/bridge-cexs/dto';

const url_provider = 'https://wallet.uphold.com/dashboard'
const provider = 7;
const currency_binance = ['ARS','AUD','BRL','CAD','HRK','CZK','DKK','AED','EUR','HDK','HUF','INR','ILS','KES','MXN','NZD','NOK','PHP','PLN','GBP','RON','SGD','SEK','CHF','USD','JPY','CNY'];

export class ListCoinsUphold {
  static adListJson(ad: ResReqUpholdRates[], currencies:CreateCurrencyDto[], coin: Coin[]) {

   let rates = [];
   ad.forEach( rate => {
    const symbol = rate.pair.split("USD").join('');
    const crypto = coin.find( c => c.symbol.toUpperCase() === symbol );
    if (rate.pair.slice(-3) === 'USD') { // criptos
      currencies.map( currency => {
        const bin = currency_binance.find( c => c === currency.currency_symbol );
        if (bin !== undefined && crypto !== undefined) {
          const cur = {
            coin: symbol,
            country: currency.country_code,
            tasa: (currency.rate_usd * parseFloat(rate.ask)),
            provider,
            url_provider,
            stable_coin: false
          };
          rates = rates.concat(cur);
        }
      });
    }
  });
    return rates;
  }

  constructor(
    public key: string,
    public tasa: number,
  ) {

  }
}
