import { ResReqBinanceRates, CreateCurrencyDto } from 'src/modules/bridge-cexs/dto';
const currency_binance = ['AED','AUD','BGN','BRL','CHF','COP','CZK','DKK','EUR','GBP','HKD','HRK','HUF','IDR','ILS','KES','KRW','KZT','MXN','NGN','NOK','NZD','PEN','PHP','PLN','RON','RUB','SAR','SEK','SGD','THB','TRY','TWD','UAH','USD','VND','ZAR','CAD','JPY','GHS','CLP','ISK','UYU','UGX','AZN'];
const url_provider = 'https://www.binance.com/es/buy-sell-crypto?channel=hzBankcard&fiat='

export class ListCoinsBinance {
  static adListJson(ad: ResReqBinanceRates[], currencies:CreateCurrencyDto[]):ListCoinsBinance[] {
    let rates = [];
    ad.forEach( rate => {
      if (rate.symbol.slice(0,3) === 'EUR') { // stablecoin
        currencies.map( currency => {
            const bin = currency_binance.find( c => c === currency.currency_symbol );
            if (bin !== undefined) {
              const cur = {
                coin: rate.symbol.split("EUR").join(''),
                country: currency.country_code,
                tasa: (currency.rate_eur / parseFloat(rate.price)),
                provider: 2,
                url_provider: url_provider+currency.currency_symbol,
                stable_coin: true
              };
              rates = rates.concat(cur);
            }
        });
      } else if (rate.symbol.slice(-3) === 'EUR') { // criptos
        currencies.map( currency => {
          const bin = currency_binance.find( c => c === currency.currency_symbol );
          if (bin !== undefined) {
            const cur = {
              coin: rate.symbol.split("EUR").join(''),
              country: currency.country_code,
              tasa: (currency.rate_eur * parseFloat(rate.price)),
              provider: 2,
              url_provider: url_provider+currency.currency_symbol,
              stable_coin: false
            };
            rates = rates.concat(cur);
          }
        });
      }
    });
    return rates
  }


  constructor(
    public symbol: string,
    public price:  string
  ) {}

}
