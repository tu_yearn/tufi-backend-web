import { currencyData } from 'src/config/data'
const url_provider = 'https://movement.bitfinex.com/deposits/card'

export class ListCoinsBitfinex {
  static adListJson(ad: any, coin:string, currency:string) {
      currency === 'CNH' ? currency = 'CNY' : '';
      coin === 'ALG' ? coin = 'ALGO' : '';
      //const bin = currencyData.find( c => c.currency_symbol === currency );
      let result = [];
      currencyData.forEach(async (bin) => {
        if (bin.currency_symbol === currency) {
            const cur = {
                coin: coin,
                country: bin.country_code,
                tasa: ad,
                provider: 4,
                url_provider: url_provider,
                stable_coin: false
            }
            result = result.concat(cur);
        }
      });
      return result;
  }


  constructor(
    public ad: any,
  ) {}

}
