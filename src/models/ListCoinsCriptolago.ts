const url_provider = 'https://criptolago.io/'
const provider = 5;
const country = 'VE';

export class ListCoinsCriptolago {
  static adListJson(ad: any) {

    let rates = [];
    for (const key in ad) {
      if (key.slice(0,3) === 'ves') {
        const coin = key.split("ves_").join('').toUpperCase();
        const stable_coin = coin === 'GLF' || coin === 'PTR' ? true : false;
        const tasa = parseFloat(ad[key]).toFixed(2);
        const cur = {
            coin,
            country,
            tasa,
            provider,
            url_provider,
            stable_coin
        };
        rates = rates.concat(cur);
      }
    }
    return rates;
  }

  constructor(
    public key: string,
    public tasa: number,
  ) {

  }
}
