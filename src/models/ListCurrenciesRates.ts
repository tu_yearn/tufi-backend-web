import { ResReqOpenexchangerates } from 'src/modules/bridge-cexs/dto';

export class ListCurrenciesRates {
  static adListJson(ad: ResReqOpenexchangerates): ListCurrenciesRates[] {
    let rates = [];
    const eur = ad.rates.EUR;
    for (const key in ad.rates) {
      const cur = {
        currency_symbol: key,
        rate_usd: ad.rates[key],
        rate_eur: ad.rates[key]/eur,
      };
      rates = rates.concat(cur);
    }
    return rates;
  }

  constructor(
    public currency_symbol: string,
    public rate_usd: number,
    public rate_eur: number,
  ) {}
}
