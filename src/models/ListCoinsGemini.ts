import { BridgeCexsService } from '../modules/bridge-cexs/bridge-cexs.service';
import { ResReqGeminiRates, CreateCurrencyDto } from 'src/modules/bridge-cexs/dto';

const currency_gemini = ['USD','AUD','CAD','GBP','HKD'];
const url_provider = 'https://exchange.gemini.com/signin'

export class ListCoinsGemini {
  static adListJson(ad: ResReqGeminiRates[], currencies:CreateCurrencyDto[]):ListCoinsGemini[] {
    let rates = [];
    ad.forEach( rate => {
      if (rate.pair.slice(-3) === 'USD') { // stablecoin
        currency_gemini.map( currency => {
            const pair = rate.pair.split("USD").join('');
            const bin = currencies.find( c => c.currency_symbol === currency );
            if (bin !== undefined) {
              const cur = {
                coin: pair === 'G' ? 'GUSD' : pair,
                country: bin.country_code === 'AQ' ? 'US' : bin.country_code,
                tasa: (bin.rate_usd * parseFloat(rate.price)),
                provider: 6,
                url_provider: url_provider,
                stable_coin: false
              };
              rates = rates.concat(cur);
            }
        });
      }
    });
    return rates
  }

  constructor(
    public symbol: string,
    public price:  string,
    private readonly bridgeCexsService: BridgeCexsService
  ) {}

}
