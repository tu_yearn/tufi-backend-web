import { ReqCoinDto } from '../modules/coins/dto/req-coin.dto';

export class ListCoinsCoingeckoMarkets {
  static adListJson(ad: ReqCoinDto) {
    return new ListCoinsCoingeckoMarkets(
      ad.id,
      ad.symbol,
      ad.name,
      ad.image,
      ad.current_price,
      ad.market_cap,
      ad.market_cap_rank,
      ad.fully_diluted_valuation,
      ad.total_volume,
      ad.high_24h,
      ad.low_24h,
      ad.price_change_24h,
      ad.price_change_percentage_24h,
      ad.market_cap_change_24h,
      ad.market_cap_change_percentage_24h,
      ad.circulating_supply,
      ad.total_supply,
      ad.max_supply,
      ad.ath,
      ad.ath_change_percentage,
      this.convertToDate(ad.ath_date),
      ad.atl,
      ad.atl_change_percentage,
      this.convertToDate(ad.atl_date),
      this.convertToDate(ad.last_updated),
      ad.sparkline_in_7d.price,
      ad.price_change_percentage_1h_in_currency,
      ad.price_change_percentage_24h_in_currency,
      ad.price_change_percentage_7d_in_currency,
    );
  }

  constructor(
    public id: string,
    public symbol: string,
    public name: string,
    public image: string,
    public current_price: number | null,
    public market_cap: number | null,
    public market_cap_rank: number | null,
    public fully_diluted_valuation: number | null,
    public total_volume: number | null,
    public high_24h: number | null,
    public low_24h: number | null,
    public price_change_24h: number | null,
    public price_change_percentage_24h: number,
    public market_cap_change_24h: number,
    public market_cap_change_percentage_24h: number,
    public circulating_supply: number | null,
    public total_supply: number | null,
    public max_supply: number | null,
    public ath: number | null,
    public ath_change_percentage: number | null,
    public ath_date: Date | null,
    public atl: number | null,
    public atl_change_percentage: number,
    public atl_date: Date | null,
    public last_updated: Date | null,
    public sparkline_in_7d: number[],
    public price_change_percentage_1h_in_currency: number | null,
    public price_change_percentage_24h_in_currency: number | null,
    public price_change_percentage_7d_in_currency: number | null,
  ) {}

  static convertToDate(date: string | null) {
      if (date === null) return new Date()
      return new Date(date);
  }
}
