import { ResReqCoinbaseRates } from 'src/modules/bridge-cexs/dto';
import { currencyData } from 'src/config/data'
const countries_coinbase = ['CA','MX','US','CL','AD','BE','BG','HR','CY','CZ','DK','EE','FI','FR','GI','GR','GG','HU','IS','IE','IM','IT','JE','LV','LI','LT','LU','MT','MC','NO','PL','PT','RO','SM','SK','SI','ES','SE','CH','GB','SG'];
const url_provider = 'https://www.coinbase.com/accounts'

export class ListCoinsCoinbase {
  static adListJson(ad: ResReqCoinbaseRates, coin:string):ListCoinsCoinbase[] {
    let rates = [];
    countries_coinbase.forEach( country => {
        const bin = currencyData.find( c => c.country_code === country );
        const cur = {
            coin: coin,
            country: country,
            tasa: ad.data.rates[bin.currency_symbol],
            provider: 3,
            url_provider: url_provider,
            stable_coin: false
        };
        rates = rates.concat(cur);
    });
    return rates
  }


  constructor(
    public ad: ResReqCoinbaseRates,
  ) {}

}
