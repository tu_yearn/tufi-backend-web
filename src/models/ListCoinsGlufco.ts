import { CreateBridgeCexDto, ResGetGlufcoRatesInterface } from 'src/modules/bridge-cexs/dto';
const url_provider = 'https://app.glufco.com/login'

export class ListCoinsGlufco {
  static adListJson(ad: ResGetGlufcoRatesInterface) {
    const glufco = new ListCoinsGlufco(
      parseFloat(ad.ves_glf_buy_2pct),
      parseFloat(ad.zelle_glf),
      parseFloat(ad.pen_glf),
      parseFloat(ad.clp_glf),
      parseFloat(ad.ecu_glf),
      parseFloat(ad.brl_glf)
    );

    return glufco.list;
  }

  constructor(
    public ves: number,
    public usd: number,
    public pen: number,
    public clp: number,
    public ecu: number,
    public brl: number,
  ) {

  }

  private get list(): CreateBridgeCexDto[] {
      const coins: CreateBridgeCexDto[] = [
          {
            coin: 'GLF',
            country: 'VE',
            tasa: this.ves,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          },
          {
            coin: 'GLF',
            country: 'US',
            tasa: this.usd,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          },
          {
            coin: 'GLF',
            country: 'PE',
            tasa: this.pen,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          },
          {
            coin: 'GLF',
            country: 'CL',
            tasa: this.clp,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          },
          {
            coin: 'GLF',
            country: 'EC',
            tasa: this.ecu,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          },
          {
            coin: 'GLF',
            country: 'BR',
            tasa: this.brl,
            provider: 1,
            url_provider: url_provider,
            stable_coin: true
          }
        ]
      return coins
  }
}
