import { Logger } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { BridgeCexsService } from './modules/bridge-cexs/bridge-cexs.service';

@WebSocketGateway(3100)
export class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(private readonly bridgeCexsService: BridgeCexsService) {}

  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('AppGateway');

  afterInit(server: Server) {
    this.logger.log('Initialized!');
      this.initLoadBridgeCexsGlufco();
      this.initUpdateCurrencyRate();
      this.initLoadBridgeCexsBinance();
      this.initLoadBridgeCexsCoinbase();
      this.initLoadBridgeCexsBitfinex();
      this.initLoadBridgeCexsCriptolago();
      this.initLoadBridgeCexsGemini();
      this.initLoadBridgeCexsUphold();
  }

  handleConnection(client: Socket): void {
    this.logger.log(`Client connected: ${client.id}`);
    const text = 'hola';
    this.server.emit('msgToClient', text);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('msgToServer')
  handleMessage(client: Socket, text: string): void {
    this.logger.log(text);
    this.server.emit('msgToClient', 'hola');
  }

  initLoadBridgeCexsGlufco() {
    this.logger.log('Actualizando tabla BridgeCexsGlufco');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getGlufcoRates();
      await this.bridgeCexsService.update(dto);
      return {
        message: 'Coins update',
      };
    }, 180000);
    return [];
  }

  initUpdateCurrencyRate() {
    this.logger.log('Actualizando Rate Currency');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getCurrencyRates();
      await this.bridgeCexsService.updateCurrencyRate(dto);
      return {
        message: 'Coins update Currency Rate',
      };
    }, 3600000);
    return [];
  }

  initLoadBridgeCexsBinance() {
    this.logger.log('Actualizando tabla BridgeCexsBinance');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getBinanceRates();
      await this.bridgeCexsService.update(dto);
      return {
        message: 'Coins update Binance',
      };
    }, 180000);
    return [];
  }

  initLoadBridgeCexsCoinbase() {
    const coin = ['ZRX','1INCH','AAVE','ALGO','FORTH','ANKR','REP','BAL','BNT','BAND','BAT','BTC','BCH','ADA','CTSI','CGLD','LINK','CVC','COMP','ATOM','CRV','DAI','DASH','MANA','EOS','ENJ','ETH','ETC','FIL','ICP','KNC','LTC','LRC','MKR','MIR','NKN','NU','NMR','OMG','OXT','OGN','MATIC','REN','SKL','XLM','STORJ','SUSHI','SNX','TRB','USDT','XTZ','GRT','UMA','USDC','UNI','WBTC','XRP','ZEC','DNT','RLC','YFI'];
    this.logger.log('Actualizando tabla BridgeCexsCoinbase');
    setInterval(async () => {
      coin.forEach(async (element) => {
        const dto = await this.bridgeCexsService.getCoinbaseRates(element);
        await this.bridgeCexsService.update(dto);
        return {
            message: 'Coins update Coinbase',
        };
      });
    }, 180000);
    return [];
  }

  initLoadBridgeCexsBitfinex() {
    const coin = ['BTC','ETH','TRX','BAT','ALG'];
    this.logger.log('Actualizando tabla BridgeCexsBitfinex');
    setInterval(async () => {
      coin.forEach(async (element) => {
        const currency = ['USD','EUR','CNH','JPY','GBP'];
        let res = [];
        for (let index = 0; index < currency.length; index++) {
            const cur = currency[index];
            const dto = await this.bridgeCexsService.getBitfinexRates(element, cur);
            res = res.concat(await dto);
        }
        const results = await Promise.all(res);
        await this.bridgeCexsService.update(results);
        return {
            message: 'Coins update Bitfinex',
        };
      });
    }, 180000);
    return [];
  }

  initLoadBridgeCexsCriptolago() {
    this.logger.log('Actualizando tabla BridgeCexsCriptolago');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getCriptolagoRates();
      await this.bridgeCexsService.update(dto);
      return {
        message: 'Coins update',
      };
    }, 180000);
    return [];
  }

  initLoadBridgeCexsGemini() {
    this.logger.log('Actualizando tabla BridgeCexsGemini');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getGeminiRates();
      await this.bridgeCexsService.update(dto);
      return {
        message: 'Coins update Gemini',
      };
    }, 180000);
    return [];
  }

  initLoadBridgeCexsUphold() {
    this.logger.log('Actualizando tabla BridgeCexsUphold');
    setInterval(async () => {
      const dto = await this.bridgeCexsService.getUpholdRates();
      await this.bridgeCexsService.update(dto);
      return {
        message: 'Uphold update',
      };
    }, 180000);
    return [];
  }
}
