export const SERVER_PORT = 'PORT';
export const JWT_SECRET = 'JWT_SECRET';
/**
 * Esta variable nos devolvera la configuracion de typeorm
 */
export const TYPEORM_CONFIG = 'database.config'
export const DATABASE_HOST = 'DATABASE_HOST';
export const DATABASE_PORT = 'DATABASE_PORT';
export const DATABASE_USERNAME = 'DATABASE_USERNAME';
export const DATABASE_PASSWORD = 'DATABASE_PASSWORD';
export const DATABASE_NAME = 'DATABASE_NAME';

export const urlLocalBitcoins = 'https://localbitcoins.com';
export const urlLocalCoinSwap = 'https://localcoinswap.com/api/v2';
export const urlPriceLocalCloinSwap = 'https://localcoinswap.com/api/v2/offers/dryrun-formula/'

export const urlGetGlufcoRates = 'https://glufco.io/api/getglfrates'
export const urlGetCurrenciesRates = 'https://openexchangerates.org/api/latest.json?app_id=52dc404334cc4bfbb58553f216cc625a'
export const urlGetBinanceRates = 'https://api.binance.com/api/v1/ticker/price'
export const urlGetGeminiRates = 'https://api.gemini.com/v1/pricefeed'
export const urlCoinbaseRates = 'https://api.coinbase.com/v2/exchange-rates?currency='
export const urlBitfinexRates = 'https://api.bitfinex.com/v2/calc/fx'
export const urlCriptolagoRates = 'https://criptolago.io/api/tasas'
export const urlUpholdRates = 'https://api.uphold.com/v0/ticker/USD'
export const tokenLocalCoinSwap = '8fc4c23866bf44b981eecba781c1dc76442b0096'
